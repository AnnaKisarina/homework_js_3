let result = ""; 
let userNum = null;

do{
    userNum = Number(prompt("Enter number"));
}
while(userNum !== parseInt(userNum));

for (let i = 0; i <= userNum; i++) {
    if (i && i % 5 === 0) {
        result += " " + i;
    }
}

if (result === '') {
    result = "Sorry, no numbers";
} 

console.log(result);